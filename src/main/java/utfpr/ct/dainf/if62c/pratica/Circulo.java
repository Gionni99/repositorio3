/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Giovanni Bandeira
 */
public class Circulo extends Elipse{
    public double raio;

    public Circulo(double eixo1) {
        super(eixo1,eixo1);
        raio=eixo1;
    }
    
    @Override
    public double getPerimetro(){
        Perimetro=2.*Math.PI*raio;
        return Perimetro;
    }
    @Override
    public double getArea() {
        Area=Math.PI*raio*raio;
        return Area;
    }
}
