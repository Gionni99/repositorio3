import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Giovanni Bandeira
 */
public class Pratica42 {

    public static void main() {
        Circulo c = new Circulo(5.);
        Elipse e = new Elipse(3.,7.);
        System.out.println("Area do Circulo:"+c.getArea());
        System.out.println("Perimetro do Circulo"+c.getPerimetro());
        System.out.println("Area de elipse:"+e.getArea());
        System.out.println("Perimetro de elipse"+e.getPerimetro());
    }
}

